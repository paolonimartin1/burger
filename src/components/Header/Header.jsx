import React from 'react';
import Navbar from '../Navbar/Navbar';
import { HeaderStyle } from './HeaderStyle';

const Header = ({modal}) => {
  return (
    <>
      <HeaderStyle>
        <Navbar modal={modal}/>
      </HeaderStyle>
    </>
  )
}

export default Header