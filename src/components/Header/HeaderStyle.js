import styled from 'styled-components';
import headerbg from 'assets/images/header_bg.png'

const HeaderStyle = styled.header`
  height: 450px;
  background-image: url(${headerbg});
  background-size: cover;
  position: relative;
  z-index: 1;
  border-radius: 0 0 40px 40px;
  &:before {
    content: "";
    height: 85%;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    background: linear-gradient(0deg, rgba(0,0,0,0) 0%, rgba(0,0,0,.8) 100%);
    z-index: -1;
  }
`;

export { HeaderStyle }