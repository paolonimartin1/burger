import React from 'react'
import { ProductCardStyle } from './ProductCardStyle'
import { Button } from 'components/Styled/Button'
import Carrito from 'assets/images/cart.svg'

const ProductCard = ({imagen,nombre,texto,precio}) => {
  return (
    <ProductCardStyle>
      <img className="foto" src={imagen} alt={nombre} />
      <div className="info">
        <h3>{nombre}</h3>
        <p>{texto}</p>
        <div className="botones">
          <span className="precio">
            <span>$</span> {precio}
          </span>
          <Button>
            <img src={Carrito} alt="Cart" />
            Select
          </Button>
        </div>        
      </div>
    </ProductCardStyle>
  )
}

export default ProductCard
