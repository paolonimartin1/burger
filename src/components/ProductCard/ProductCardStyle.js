import styled from 'styled-components';
import { colors } from 'components/Styled'

const ProductCardStyle = styled.div`
  background: #fff;
  box-shadow: 0px 0px 0px 1px rgba(0, 0, 0, 0.04), 0px 4px 25px rgba(0, 0, 0, 0.05), 0px 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 8px;
  overflow: hidden;

  .foto {
    height: 128px;
    width: 100%;
    object-fit: cover;
    object-position: center;
  }

  .info {
    padding: 16px;
    text-align: left;

    h3 {
      font-size: 1.5rem;
      font-weight: 400;
      margin-top: 0;
      margin-bottom: 10px;
    }
    p {
      color: #6C707B;
      font-size: 1.2rem;
      margin: 0;
    }

    .botones {
      margin-top: 22px;
      display: flex;
      justify-content: space-between;

      .precio {
        text-align: center;
        font-size: 1.2rem;
        line-height: 1;
        color: #000;
        border-radius: 4px;
        padding:10px 23px;
        display: inline-block;
        font-size: 1.2rem;
        width: 108px;
        background-color: ${colors.gris};

        span {
          opacity: 0.4;
          margin-right: 3px;
        }
      }
    }    
  }
`;

export { ProductCardStyle }