import React from "react";
import logo from 'assets/images/logo.png';
import NavbarStyle from './NavbarStyle'
import BigButton from "components/Styled/BigButton/BigButton";
import bag from 'assets/images/bag.svg'
import { useState } from "react";


function Navbar({modal}) {
  const ToggleModal = () => modal = !modal;
  return(
    <>
    <div className="container">
      <NavbarStyle>
        <img src={logo} className="logo" alt="Simpler Burger" width="175"/>

        <div className="view-order">
          <BigButton
          icono={bag}
          texto="View order"
          precio="11.99"
          />
          {/* <button onClick={() => ToggleModal}>Probando</button> */}
        </div>
      </NavbarStyle>
    </div>

    </>
  );
}


export default Navbar