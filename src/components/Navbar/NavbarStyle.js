import styled from "styled-components";

const NavbarStyle = styled.div`
  display: flex;
  justify-content: space-between;

  img {
    image-rendering: pixelated;
  }

  .view-order {
    min-width: 370px;
    background: #fff;
    box-shadow: 0px 0px 0px 5px rgba(0, 0, 0, 0.06), 0px 0px 0px 1px rgba(0, 0, 0, 0.25);
    border-radius: 0px 0px 10px 10px;
    padding: 16px;
  }
`;

export default NavbarStyle