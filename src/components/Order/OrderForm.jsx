import {OrderBlock, FormBlock} from './OrderStyle'
import RadioButton from 'components/Styled/RadioButton/RadioButton'
import Checkbox from 'components/Styled/Checkbox/Checkbox'

const OrderForm = ({imagen, titulo, texto}) => {
  return (
    <>
    <OrderBlock>
      <div className="inner">

        <div className="grid-1">
        <FormBlock>
          <h3 className="titulo">Size</h3>
          <div className="options">
            <RadioButton label="Small" name="size"/>
            <RadioButton label="Big (+$2.00)" name="size"/>
          </div>
        </FormBlock>
        <FormBlock>
          <h3 className="titulo">Soda flavour</h3>
          <div className="options">
            <RadioButton label="Cola" name="soda"/>
            <RadioButton label="Lemon" name="soda"/>
            <RadioButton label="Grapefruit" name="soda"/>
            <RadioButton label="Water" name="soda"/>
            <RadioButton label="Orange" name="soda"/>
          </div>
        </FormBlock>
        </div>

        <FormBlock>
          <h3 className="titulo">Toppings</h3>
          <div className="options">
            <Checkbox label="Mayonnaise" name=""/>
            <Checkbox label="Ketchup" name=""/>
            <Checkbox label="Lettuce" name=""/>
            <Checkbox label="Tomato" name=""/>
            <Checkbox label="Pickles" name=""/>
            <Checkbox label="Onion" name=""/>
            <Checkbox label="Bread" name=""/>
          </div>
        </FormBlock>

      </div>  
    </OrderBlock>
    </>
  )
}

export default OrderForm
