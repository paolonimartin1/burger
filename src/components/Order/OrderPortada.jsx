import React from 'react'
import { OrderPortadaStyle } from './OrderStyle'

const OrderPortada = ({imagen, titulo, texto}) => {
  return (
    <OrderPortadaStyle>
      <img src={imagen} alt={titulo} />
      <div className="info">
        <h2 className="titulo">{titulo}</h2>
        <p className="descripcion">{texto}</p>
      </div>
    </OrderPortadaStyle>
  )
}

export default OrderPortada
