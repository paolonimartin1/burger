import styled from "styled-components";
import { breakpoints } from "components/Styled";

const OrderPortadaStyle = styled.div`
  img {
    width: 100%;
    height: 252px;
    object-fit: cover;
    object-position: center;
    border-radius: 8px 8px 0 0;
  }

  .info {
    background: #fff;
    padding: 16px;
    text-align: left;
    border-radius: 0 0 8px 8px;

    .titulo {
      font-size: 1.6rem;
      font-weight: 400;
      color: #000;
      margin-top: 0;
      line-height: 1;
    }

    .descripcion {
      color: #6C707B;
      font-size: 1.2rem
    }
  }
`;

const OrderBlock = styled.div`
  background: #fff;
  border-radius: 8px;
  margin: 16px 0;

  .inner {
    padding: 16px;
  }

  .grid-1 {
    @media (min-width:${breakpoints.sm}px) {
      display: grid;
      grid-template-columns: 1fr 2fr;
      grid-gap: 4rem;
      margin-bottom: 30px;
    }
  }
`;

const FormBlock = styled.div`
  .titulo {
    font-size: 1.6rem;
    font-weight: 400;
    padding-bottom: 8px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.12);
    margin-bottom: 16px;
  }
  .options {
    display: flex;
    flex-wrap: wrap;
  }
`;


export {OrderPortadaStyle, OrderBlock, FormBlock}

