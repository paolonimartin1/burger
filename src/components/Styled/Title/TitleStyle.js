import styled from "styled-components";
import { colors } from 'components/Styled'

export const Titulo = styled.h1`
  font-family: 'Cocogoose Pro';
  font-weight: 300;
  color: #fff;
  font-size: 4rem;
  margin: 0;
  text-shadow: 5px 5px 0 ${colors.lila};
  margin-bottom: 50px;
  text-align: center;
`;

export const Subtitulo = styled.h2`
  text-align: center;
  font-family: 'Antonio', sans-serif;
  color: ${colors.lila};
  font-size: 1.6rem;
  position: relative;
  text-transform: uppercase;
  margin:0;

  &:before {
    content: "";
    height: 1px;
    width: 30px;
    background-color: ${colors.lila};
    display: inline-block;
    margin-right: 9px;
    position: relative;
    top: -5px;
  }
  &:after {
    content: "";
    height: 1px;
    width: 30px;
    background-color: ${colors.lila};
    display: inline-block;
    margin-left: 9px;
    position: relative;
    top: -5px;
  }
`;