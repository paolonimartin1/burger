import React from 'react'
import { Fonts } from "components/Fonts";
import {Titulo, Subtitulo} from './TitleStyle';

const Title = ({subtitulo, titulo}) => {
  return (
    <>
      <Fonts/>
      <Subtitulo>{subtitulo}</Subtitulo>
      <Titulo>{titulo}</Titulo>
    </>
  )
}

export default Title
