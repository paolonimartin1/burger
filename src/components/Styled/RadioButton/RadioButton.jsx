import React from 'react'
import { RadioButtonStyle } from './RadioButtonStyle'

const RadioButton = ({label, name}) => {
  return (
    <>
      <RadioButtonStyle>{label}
        <input type="radio" checked="checked" name={name}/>
        <span class="checkmark"></span>
      </RadioButtonStyle>
    </>
  )
}

export default RadioButton
