import styled from "styled-components";
import { colors } from 'components/Styled'

const BigButtonStyle = styled.button`
  background: ${colors.rojo};
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 14px;
  height:40px;
  border-radius: 8px;
  width: 100%;
  color: #fff;
  border:0;
  outline: 0;

  &:hover {
    cursor: pointer;
    background: ${colors.verde};
  }
`;

export default BigButtonStyle;