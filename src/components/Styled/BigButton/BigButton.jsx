import React from 'react'
import BigButtonStyle from './BigButtonStyle'

const BigButton = ({icono, texto, precio}) => {
  return (
    <BigButtonStyle>
      <span><img src={icono} alt="Cart icon" /></span>
      <span>{texto}</span>
      <span>${precio}</span>
    </BigButtonStyle>
  )
}

export default BigButton
