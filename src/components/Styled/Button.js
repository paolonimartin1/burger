import styled from "styled-components";
import { colors } from 'components/Styled'


export const Button = styled.button.attrs(props => ({
  className: props.Classname,
}))`
  border: none;
  outline: none;
  font-size: 1.2rem;
  line-height: 1;
  color: #fff;
  border-radius: 4px;
  padding:10px 23px;
  display: flex;
  align-items: center;
  font-size: 1.2rem;
  width: 108px;
  color: ${(props) => props.color || colors.white};
  background-color: ${(props) => props.bgColor || colors.verde};
  justify-content: ${(props) => props.justifyContent || 'space-around'};

  &:hover {
    background-color: ${(props) => props.bgHover || colors.rojo};
    cursor: pointer;
  }
  &:disabled,
  .disabled {
    color: ${colors.negro};
    background-color: ${colors.gris};
  }

  &.remove {
    background: ${colors.rojo};
    &:hover {
      background: ${colors.negro}
    }
  }
`;
