import React from 'react'
import { CheckboxStyle } from './CheckboxStyle'

const Checkbox = ({label, name}) => {
  return (
    <>
      <CheckboxStyle>{label}
        <input type="checkbox" name={name}/>
        <span class="checkmark"></span>
      </CheckboxStyle>
    </>
  )
}

export default Checkbox