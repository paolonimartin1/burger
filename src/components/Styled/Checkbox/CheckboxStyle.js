import styled from "styled-components";
import { colors } from "components/Styled";
import check from "assets/images/check.svg"

const CheckboxStyle = styled.label`
  display: block;
  position: relative;
  padding-left: 25px;
  margin: 10px 10px 0 10px;
  font-size: 1.2rem;
  line-height: 16px;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;

/* Create a custom radio button */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 16px;
  width: 16px;
  border: 2px solid ${colors.rojo};
  border-radius: 4px;
  display: flex;
  justify-content: center;
  align-items: center;
  line-height: 0;

  &:after {
    content: url(${check});
    width: 10px;
    height: 10px;
    display: none;
  }
}

input {
  display: none;

  &:checked ~ .checkmark {
    background-color: ${colors.rojo};
  }
  &:checked ~ .checkmark:after {
    display: block;
  }
}

&:hover input ~ .checkmark {
  opacity: 0.7;
}

`;

export {CheckboxStyle}