const breakpoints = {
  xs: 576,
  sm: 720,
  md: 992,
  lg: 1200,
  xlg: 1400,
};


const colors = {
  rojo: "#F01C4F",
  lila: '#4C3260',
  verde: '#5AD88C',
  background: '#FFF9EE',
  gris: '#EFF0F2',
  negro: '#000',
};


export {breakpoints, colors}