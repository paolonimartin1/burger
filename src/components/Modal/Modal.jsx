import React from 'react';
import ModalStyle from './ModalStyle';
import closeIcon from 'assets/images/close.svg'

const Modal = ({children, modal}) => {
  if (modal) {
    document.body.classList.add("modal-open");
  }
  return (
    <>
    { modal ?
    <ModalStyle>
      <div className="modal">
        <div className="content">
          <button class="close" onClick={modal=false}> X </button>
          {children}
        </div>
      </div>
    </ModalStyle>
      : null
    }
   </>
  );
};

export default Modal;