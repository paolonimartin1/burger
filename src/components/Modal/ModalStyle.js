import styled from "styled-components";

const ModalStyle = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  bottom: 0;
  background: rgba(0,0,0,0.7);
  z-index: 99;
  display: flex;
  justify-content: center;
  align-items: center;
  backdrop-filter: blur(5px);
  overflow-y: scroll;

  .modal {
    width: 50vw;
    .content {
      position: relative;
    }
  }

  .close {
    background: transparent;
    border: 0;
    display: inline-block;
    position: absolute;
    top: 10px;
    right: 10px;
    z-index: 99;
    color: #fff;
    font-size: 2rem;
  }

`;

export default ModalStyle