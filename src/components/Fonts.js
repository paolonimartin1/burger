import { createGlobalStyle } from 'styled-components';
import CocogooseProBlack from 'assets/fonts/Cocogoose-Pro-Block-Shadow-black-Trial.woff';
import CocogoosePro from 'assets/fonts/Cocogoose-Pro-Letterpress-Trial.woff';

const Fonts = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css2?family=Antonio&display=swap');
  
  @font-face {
    font-family: 'Cocogoose Pro';
    src: url(${CocogoosePro}) format('woff');
    font-weight: 300;
    font-style: normal;
  }

  @font-face {
    font-family: 'Cocogoose Pro';
    src: url(${CocogooseProBlack}) format('woff');
    font-weight: 900;
    font-style: normal;
  }

`;

export {Fonts};