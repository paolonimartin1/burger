import React from 'react'
import {FooterStyle} from './FooterStyle'
import logo from 'assets/images/logo.png';

const Footer =() =>{
  return (
    <FooterStyle>
      <img src={logo} alt="Simpler Burger" />
    </FooterStyle>
  )
}

export default Footer
