import styled from 'styled-components';
import { colors } from 'components/Styled'

export const FooterStyle = styled.footer`
  background: ${colors.negro};
  height: 153px;
  display: flex;
  justify-content: center;
  align-items: center;
  img {
    image-rendering: pixelated;
    width: 170px
  }
`;