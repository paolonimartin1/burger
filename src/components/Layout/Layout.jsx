import React from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import {Main} from './LayoutStyle';

const Layout =({children, modal}) =>{
  return (
    <>
      <Header modal={modal}/>
      <Main>
        {children}
      </Main>
      <Footer/>
    </>
  )
}

export default Layout
