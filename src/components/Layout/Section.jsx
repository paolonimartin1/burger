import React from 'react';
import { SectionStyle } from './SectionStyle';

const Section =({children, bgColor, padding}) =>{
  return (
    <>
      <SectionStyle bgColor={bgColor} padding={padding} >
        {children}
      </SectionStyle>
    </>
  )
}

export default Section
