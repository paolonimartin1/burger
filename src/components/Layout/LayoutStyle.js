import styled from 'styled-components';
import bg1 from 'assets/images/bg1.png'

export const Main = styled.main`
  position: relative;
  &:before {
    content: "";
    position: absolute;
    z-index: 0;
    background-image: url(${bg1});
    background-repeat: repeat-y;
    left: -400px;
    top: -200px;
    width: 100%;
    height: 200vh;

  }
`;