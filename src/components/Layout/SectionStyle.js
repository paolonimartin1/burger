import styled from "styled-components";
import {breakpoints} from 'components/Styled'

const SectionStyle = styled.section`
  padding: ${(props) => props.padding || '78px 0 130px 0'};
  
  position: relative;
  &:before {
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background-color: ${(props) => props.bgColor || '#fff'};
    border-radius: 0 0 40px 40px;
    z-index: -1;
  }
 .grilla {
    display: grid;
    grid-template-columns: repeat(4, minmax(0, 1fr));
    grid-gap: 3rem;
    @media (max-width:${breakpoints.lg}px) {
      grid-template-columns: repeat(3, minmax(0, 1fr));
    }
    @media (max-width:${breakpoints.md}px) {
      grid-template-columns: repeat(2, minmax(0, 1fr));
    }
    @media (max-width:${breakpoints.xs}px) {
      grid-template-columns: repeat(1, minmax(0, 1fr));
    }
  }
`;

export { SectionStyle }