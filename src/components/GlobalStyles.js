import { createGlobalStyle } from 'styled-components';
import { breakpoints } from './Styled';

const GlobalStyles = createGlobalStyle`
  html {
    box-sizing: border-box;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;;
    font-size: 10px;
    text-align: left;
  }
  * {
    box-sizing: border-box;
  }

  h1,h2,h3,h4 {
    margin: 1rem 0 0.5rem 0;
  }
  hr {
    border: 1px solid #ddd;
  }

  .container {
    margin:auto;
    @media (min-width:${breakpoints.md}) {
      width: ${breakpoints.md}
    }
    @media (min-width:${breakpoints.xlg}px) {
      width: ${breakpoints.lg}px
    }
    @media (min-width:${breakpoints.xlg}) {
      width: ${breakpoints.xlg}
    }
  }
  
`;

export {GlobalStyles};