import './App.css';
import { GlobalStyles } from './components/GlobalStyles';
import { colors } from 'components/Styled';
import Layout from './components/Layout/Layout';
import Section from 'components/Layout/Section';
import Title from 'components/Styled/Title/Title';
import ProductCard from 'components/ProductCard/ProductCard';
import Hamburguesa1 from 'assets/images/hamburguesa1.png'
import Hamburguesa2 from 'assets/images/hamburguesa2.png'
import Hamburguesa3 from 'assets/images/hamburguesa3.png'
import Hamburguesa4 from 'assets/images/hamburguesa4.png'
import Modal from 'components/Modal/Modal';
import OrderPortada from 'components/Order/OrderPortada';
import OrderForm from 'components/Order/OrderForm';
import { useState } from "react";

function App() {
  const [modal, setModal] = useState(false);
  const ToggleModal = () => setModal(!modal);

  return (
    <div className="App">
      <GlobalStyles/>

      <Modal modal={modal}>
        <OrderPortada imagen={Hamburguesa1} titulo="Double Burger" texto="Double jucy delicious burger. Choose your toppings."/>
        <OrderForm/>
      </Modal>

      <button onClick={() => ToggleModal()}>Abrir Modal</button>

      <Layout modal={modal}>

        <Section bgColor={colors.background}>
          <div className="container">
              <Title
              titulo="COMBO"
              subtitulo="Complete for you"
              />

              <div className="grilla">
                <ProductCard
                nombre="Combo Burger"
                texto="Simple but jucy burger. Choose your toppings."
                precio="9.99"
                imagen={Hamburguesa1}
                />

                <ProductCard
                nombre="Combo Double Burger"
                texto="Double jucy delicious burger. Choose your toppings."
                precio="10.99"
                imagen={Hamburguesa2}
                />

                <ProductCard
                nombre="Combo Burger + Cheese"
                texto="What’s a burger without cheese? Choose your toppings."
                precio="11.99"
                imagen={Hamburguesa3}
                />

                <ProductCard
                nombre="Combo Double Burger + Cheese"
                texto="What’s a burger without melted delicious DOUBLE cheese?"
                precio="15.49"
                imagen={Hamburguesa4}
                />
              </div>
          </div>
        </Section>

        <Section>
          <div className="container">
              <Title
              titulo="BURGERS"
              subtitulo="Delicious"
              />

              <div className="grilla">
                <ProductCard
                nombre="Burger"
                texto="Simple but jucy burger. Choose your toppings."
                precio="6.25"
                imagen={Hamburguesa1}
                />

                <ProductCard
                nombre="Double Burger"
                texto="Double jucy delicious burger. Choose your toppings."
                precio="8.49"
                imagen={Hamburguesa2}
                />

                <ProductCard
                nombre="Burger + Cheese"
                texto="What’s a burger without cheese? Choose your toppings."
                precio="9.00"
                imagen={Hamburguesa3}
                />

                <ProductCard
                nombre="Double Burger + Cheese"
                texto="What’s a burger without melted delicious DOUBLE cheese?"
                precio="13.00"
                imagen={Hamburguesa4}
                />
              </div>
          </div>
        </Section>

        <Section padding={'0 0 78px 0'}>
          <div className="container">
              <Title
              titulo="OTHERS"
              subtitulo="More?"
              />

              <div className="grilla">
                <ProductCard
                nombre="Fries"
                texto="Crunchy unique fries."
                precio="3.49"
                imagen={Hamburguesa1}
                />

                <ProductCard
                nombre="Soda"
                texto="Accompany your burger with a soda."
                precio="1.00"
                imagen={Hamburguesa2}
                />
              </div>
          </div>
        </Section>        

      </Layout>
    </div>
  );
}

export default App;
